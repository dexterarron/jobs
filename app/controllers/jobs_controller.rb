class JobsController < ApplicationController
  include ApplicationHelper

  before_action :set_job, only: [:show, :edit, :update, :destroy, :remove_document, :mark_as_filled]
  before_action :authenticate_user!, only: [:edit, :update, :destroy, :new, :remove_document, :mark_as_filled]

  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = Job.all
    # @active_jobs = Job.active
    @categories = Category.all
    @departments = Department.all
    @pagy, @active_jobs = pagy(Job.all, items: 10) #change this to active after testing
    @active_jobs_count = Job.all.count #change this to active after testing
    @active_categories = @active_jobs.pluck(:category_id).uniq.map { |id| Category.find(id) }
    @active_jobs_salaries = @active_jobs.pluck(:start_salary, :end_salary)
    @active_departments = @active_jobs.pluck(:department_id).uniq.map { |id| Department.find(id) }
    @filled_jobs = Job.by_filled
    @not_filled_jobs_count = Job.all.count - @filled_jobs.count

    # Filter 
    job_filter

    # Search 
    @active_jobs = @active_jobs.by_title(params[:search][:title]) if params[:search].present?
  end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
  end

  # GET /jobs/new
  def new
    @job = Job.new
  end

  # GET /jobs/1/edit
  def edit

  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(job_params)

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render :show, status: :created, location: @job }
      else
        format.html { render :new }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /jobs/1
  # PATCH/PUT /jobs/1.json
  def update
    respond_to do |format|
      if @job.update(job_params)
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { render :show, status: :ok, location: @job }
      else
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job.destroy
    respond_to do |format|
      format.html { redirect_to jobs_url, notice: 'Job was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def job_filter
    if !admin? or !editor?
      @active_jobs = @active_jobs.by_category(params[:category]) if params[:category].present?
      @active_jobs = @active_jobs.by_department(params[:department]) if params[:department].present?
      @active_jobs = @active_jobs.by_start_and_end_salary(params[:start_salary], params[:end_salary]) if params[:start_salary].present? and params[:end_salary].present?
    else
      @jobs = @jobs.by_category(params[:category]) if params[:category].present?
      @jobs = @jobs.by_department(params[:department]) if params[:department].present?    
    end
  end

  # Remove Document from job
  def remove_document
    @document_to_remove = params[:document].to_i

    remain_documents = @job.documents 
    deleted_documents = remain_documents.delete_at(@document_to_remove) 
    @job.documents = remain_documents

    respond_to do |format|
      if @job.save
        format.html { render :edit }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # Mark job as filled
  def mark_as_filled
    respond_to do |format|
      @job_params = params[:job]
      @date = Date.new(@job_params["date_filled(1i)"].to_i, @job_params["date_filled(2i)"].to_i, @job_params["date_filled(3i)"].to_i)
      @job.update_attribute(:date_filled, @date) and @job.update_attribute(:filled, true)

      if @job.save(:validate => true)
        format.html { redirect_to @job, notice: "Marked as filled" }
        format.json { head :no_content }
      else
        format.html { render :show, alert: "Date filled can't be before deadline or publish_date" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_job
      @job = Job.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def job_params
      params.require(:job).permit(
        :title, 
        :department_id,
        :category_id, 
        :description, 
        :start_salary, 
        :end_salary, 
        :start_grade, 
        :end_grade, 
        :allowance, 
        :publish_date, 
        :ends_on, 
        :filled, 
        :date_filled,
        :documents => [])
    end
end
