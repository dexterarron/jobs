class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :timeoutable, :timeout_in => 1.hour
         
  validates :password,
            :confirmation => true,
            :length => { :minimum => 6 }

  def role
  	if admin == true
  		"Admin"
  	elsif editor == true
  		"Editor"
  	end
  end
end
