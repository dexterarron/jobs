class Job < ApplicationRecord
	belongs_to :department
	has_one :category

	validates :title, presence: true
	validates :category_id, presence: true
	validates :department, presence: true
	validates :start_salary, presence: true
	validates :end_salary, presence: true
	validates :description, presence: true
	validates :start_grade, presence: true
	validates :end_grade, presence: true
	validates :publish_date, presence: true
	validates :ends_on, presence: true

	validates_date :publish_date, before: :ends_on, on_or_after: lambda { Date.today }
	validates_date :ends_on, after: :publish_date
	validates_date :date_filled, after: :ends_on, after: :publish_date, allow_blank: true

	mount_uploaders :documents, DocumentUploader # Tells rails to use this uploader for this model.

	scope :active, -> { where('ends_on > ?', Date.today).where(filled: false).where('publish_date <= ?', Date.today) }
	scope :by_category, ->(category_id) { where(category_id: category_id) }
	scope :by_title, ->(title) { where("title ILIKE ?", "%#{title}%") }
	scope :by_department, ->(department) { where(department_id: department) }
	scope :by_start_and_end_salary, ->(start_salary, end_salary) { where(start_salary: start_salary, end_salary: end_salary) }
	scope :by_filled, -> { where(filled: true) }

	def days_left
		left = (ends_on - publish_date).to_i
		if left > 0
			left
		elsif left == 0
			'Last Day'
		elsif left < 0
			'Closed'
		end
	end

	def active?
		ends_on > Date.today
	end

	def filled_status?
		if filled?
			"Filled"
		else 
			"Not Filled"
		end
	end
end
