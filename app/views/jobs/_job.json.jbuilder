json.extract! job, :id, :title, :references, :references, :description, :start_salary, :end_salary, :start_grade, :end_grade, :allowance, :publich_date, :ends_on, :filled, :date_filled, :created_at, :updated_at
json.url job_url(job, format: :json)
