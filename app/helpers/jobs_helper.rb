module JobsHelper

	def active_job_categories_by_id(category_id)
		Job.all.by_category(category_id) #cahnge to active
	end

	def format_salary(job)
		"$#{job.start_salary} - $#{job.end_salary}"
	end

	def format_grade(job)
		"#{job.start_grade} - #{job.end_grade}"
	end

	# Format for catigories, to show the count if the count is more than 0
	# if not it removes the ()
	def format_count(count)
		if count != 0
			"(#{count})"
		else
			""
		end
	end

end
