module ApplicationHelper
	include Pagy::Frontend

	def admin?
		current_user and current_user.admin?
	end

	def editor?
		current_user and current_user.editor?
	end

end
