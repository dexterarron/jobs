class ChangeColumn < ActiveRecord::Migration[5.2]
  def change
  	rename_column :jobs, :attachment_data, :documents
  end
end
