class AddAttachmentToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :attachment_date, :text
  end
end
