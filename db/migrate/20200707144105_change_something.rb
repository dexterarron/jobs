class ChangeSomething < ActiveRecord::Migration[5.2]
  def change
  	remove_column :jobs, :documents
  	add_column :jobs, :documents, :text, array: true, default: []
  end
end
