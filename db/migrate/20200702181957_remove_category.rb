class RemoveCategory < ActiveRecord::Migration[5.2]
  def change
  	remove_column :jobs, :categories_id
  	add_column :jobs, :category_id, :bigint
  end
end
