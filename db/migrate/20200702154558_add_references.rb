class AddReferences < ActiveRecord::Migration[5.2]
  def change
  	add_reference :departments, :jobs, foreign_key: true
  	add_reference :jobs, :categories, foreign_key: true
  end
end
