class RenameColumn < ActiveRecord::Migration[5.2]
  def change
  	rename_column :jobs, :attachment_date, :attachment_data
  end
end
