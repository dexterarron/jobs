class ChangeTypeAgain < ActiveRecord::Migration[5.2]
  def change
  	remove_column :jobs, :documents
  	add_column :jobs, :documents, :text
  end
end
