Rails.application.routes.draw do
  devise_for :users, :skip => [:registrations]
  as :user do
    get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put 'users' => 'devise/registrations#update', :as => 'user_registration'
  end

  resources :users
  resources :departments
  resources :categories

  resources :jobs do
    member do 
      get :remove_document
      patch :mark_as_filled
    end
  end

  root :to => 'jobs#index'
end
